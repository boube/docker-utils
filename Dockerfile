FROM debian:11-slim
RUN apt-get update && \
      apt-get -y install openssh-client dnsutils vim nmap less mtr-tiny bash-completion net-tools curl wget git  psmisc netcat-openbsd && \
      apt-get clean 
